import {Routes, RouterModule} from "@angular/router";
import {ProductsComponent, HomeComponent, AdminComponent, ProductDetailComponent} from "./index";


const APP_ROUTES: Routes = [
    {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
    },
    {
        path: "home",
        component: HomeComponent
    },
    {
        path: "admin",
        component: AdminComponent
    },
    {
        path: "products",
        component: ProductsComponent
    }
];

export const routing = RouterModule.forRoot(APP_ROUTES);