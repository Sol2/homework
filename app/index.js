"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require("./admin/admin.component"));
__export(require("./home/home.component"));
__export(require("./products/products.component"));
__export(require("./products/product-detail/product-detail.component"));
//# sourceMappingURL=index.js.map