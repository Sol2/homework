export * from "./admin/admin.component";
export * from "./home/home.component";
export * from "./products/products.component";
export * from "./products/product-detail/product-detail.component";