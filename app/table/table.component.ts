import {Component, OnInit, EventEmitter} from '@angular/core';
import {ProductsService} from "../shared/products.service";


@Component({
    moduleId: module.id,
    selector: 'app-table',
    templateUrl: 'table.component.html',
    styleUrls: ['table.component.css'],
    inputs: ['rows'],
    outputs: ['deleteRow']
})
export class TableComponent implements OnInit {
    deleteRow: EventEmitter<any> = new EventEmitter();
    products: {id: number, name: string, price: number, category: string}[];
    categories: string[];
    rows: number;
    selectedCategory: string;

    constructor(private _productsService: ProductsService) {
    }

    ngOnInit() {
        this.products = this._productsService.getProducts().slice(0, this.rows);
        this.categories = this._productsService.getCategories();
        this.selectedCategory = this.categories[0];
    }

    removeRow(id) {
        this.products = this.products.filter(product => {
            return product.id != id;
        });
        this.deleteRow.emit(id);
        return false;
    }
}