"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var products_service_1 = require("../../shared/products.service");
var CategoryFormComponent = (function () {
    function CategoryFormComponent(_productsService) {
        this._productsService = _productsService;
    }
    CategoryFormComponent.prototype.ngOnInit = function () {
        this.categories = this._productsService.getCategories();
    };
    CategoryFormComponent.prototype.addProduct = function () {
        if (!this.idProduct || !this.nameProduct || !this.priceProduct || !this.categoryProduct)
            return;
        this.products.push({
            id: this.idProduct,
            name: this.nameProduct,
            price: this.priceProduct,
            category: this.categoryProduct
        });
    };
    CategoryFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-form-component',
            templateUrl: 'product-form.component.html',
            styleUrls: ['product-form.component.css'],
            inputs: ['products'],
        }), 
        __metadata('design:paramtypes', [products_service_1.ProductsService])
    ], CategoryFormComponent);
    return CategoryFormComponent;
}());
exports.CategoryFormComponent = CategoryFormComponent;
//# sourceMappingURL=product-form.component.js.map