import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../shared/products.service";

@Component({
    moduleId: module.id,
    selector: 'app-form-component',
    templateUrl: 'product-form.component.html',
    styleUrls: ['product-form.component.css'],
    inputs: ['products'],
})
export class CategoryFormComponent implements OnInit {
    categories: string[];
    products: {id: number, name: string, price: number, category: string}[];
    idProduct: number;
    nameProduct: string;
    priceProduct: number;
    categoryProduct: string;

    constructor(private _productsService: ProductsService) {

    }

    ngOnInit() {
        this.categories = this._productsService.getCategories();
    }

    addProduct() {
        if (!this.idProduct || !this.nameProduct || !this.priceProduct || !this.categoryProduct) return;
        this.products.push({
            id: this.idProduct,
            name: this.nameProduct,
            price: this.priceProduct,
            category: this.categoryProduct
        });
    }
}