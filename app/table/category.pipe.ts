import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'categoryPipe',
    pure: false
})
export class CategoryPipe implements PipeTransform {
    transform(value: any, args?: any) {
        return value.filter(item => {
            return item.category == args;
        });
    }

}
