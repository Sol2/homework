"use strict";
var router_1 = require("@angular/router");
var index_1 = require("./index");
var APP_ROUTES = [
    {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
    },
    {
        path: "home",
        component: index_1.HomeComponent
    },
    {
        path: "admin",
        component: index_1.AdminComponent
    },
    {
        path: "products",
        component: index_1.ProductsComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(APP_ROUTES);
//# sourceMappingURL=app.routes.js.map