// Определение компонента app.component

// импорт декоратора Component из модуля @angular/core
import { Component } from '@angular/core';

const VALUES = [
  "Hello World", "Привет Мир","Привіт Світ", "Hola Mundo", "Bonjour le monde", "Hallo Welt", "Ciaomondo", "Witaj świecie", "Hej världen", "Pozdravljen, svet", "Прывітанне Сусвет"
];
// Применение декоратора Component для класса AppComponent
// Декоратор используется для присвоения метаданных для класса AppComponent
@Component({
  moduleId: module.id,
  selector: 'my-app',                       // Селектор, который определяет какой элемент DOM дерева будет представлять компонент.
  templateUrl: 'app.component.html'
})
export class AppComponent {
  values: string[];
  constructor(){
    this.values = VALUES;
  }
  showDeleteRow(id) {
    console.log(id);
  }
} // Класс определяющий поведение компонента
