import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {CategoryPipe} from "./table/category.pipe";

import {AppComponent}   from './app.component';
import {TableComponent} from "./table/table.component";
import {CategoryFormComponent} from "./table/product-form/product-form.component";
import {HomeComponent, AdminComponent} from "./index";

import {ProductsService} from "./shared/products.service";

import {routing} from "./app.routes";
import {ProductsModule} from "./products/products.module";

@NgModule({
    imports: [BrowserModule, FormsModule, CommonModule, routing],
    declarations: [
        AppComponent,
        TableComponent,
        CategoryPipe,
        CategoryFormComponent,
        HomeComponent,
        AdminComponent
    ],
    bootstrap: [AppComponent],
    providers: [ProductsService],
    exports: [ProductsModule]
})

export class AppModule {
}
