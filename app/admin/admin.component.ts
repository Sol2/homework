import {Component, OnInit} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'admin-home',
    templateUrl: 'admin.component.html',
    styleUrls: ['admin.component.css']
})

export class AdminComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}