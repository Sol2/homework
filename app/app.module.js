"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var category_pipe_1 = require("./table/category.pipe");
var app_component_1 = require('./app.component');
var table_component_1 = require("./table/table.component");
var product_form_component_1 = require("./table/product-form/product-form.component");
var index_1 = require("./index");
var products_service_1 = require("./shared/products.service");
var app_routes_1 = require("./app.routes");
var products_module_1 = require("./products/products.module");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, common_1.CommonModule, app_routes_1.routing],
            declarations: [
                app_component_1.AppComponent,
                table_component_1.TableComponent,
                category_pipe_1.CategoryPipe,
                product_form_component_1.CategoryFormComponent,
                index_1.HomeComponent,
                index_1.AdminComponent
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [products_service_1.ProductsService],
            exports: [products_module_1.ProductsModule]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map