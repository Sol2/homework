import { Injectable } from "@angular/core";
import { Router, Resolve, ActivatedRouteSnapshot } from "@angular/router";
import {ProductsService} from "../shared/products.service";

@Injectable()
export class ProductDetailsResolve implements Resolve {
    constructor(private _productsService: ProductsService,  private _router: Router) { }

    resolve(route: ActivatedRouteSnapshot) {
        let id = +route.params["id"];
        let product = this._productsService.getProduct(id);
        if(product) {
            return product;
        } else {
            this._router.navigate(["/products"]);
            return false;
        }
    }
}
