import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'app-product-detail',
    templateUrl: 'product-detail.component.html',
    styleUrls: ['product-detail.component.css']
})

export class ProductDetailComponent implements OnInit {

    product: {id: number, name: string, price: number, category: string};

    constructor(private _activatedRoute: ActivatedRoute, private _router: Router) {
    }

    ngOnInit() {
        this._activatedRoute.data.forEach((data: { product }) => {
            this.product = data.product;
        });
    }

    goToProductsList() {
        this._router.navigate(["../"], { relativeTo: this._activatedRoute });
        return false;
    }
}