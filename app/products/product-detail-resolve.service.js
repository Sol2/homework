"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var products_service_1 = require("../shared/products.service");
var ProductDetailsResolve = (function () {
    function ProductDetailsResolve(_productsService, _router) {
        this._productsService = _productsService;
        this._router = _router;
    }
    ProductDetailsResolve.prototype.resolve = function (route) {
        var id = +route.params["id"];
        var product = this._productsService.getProduct(id);
        if (product) {
            return product;
        }
        else {
            this._router.navigate(["/products"]);
            return false;
        }
    };
    ProductDetailsResolve = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [products_service_1.ProductsService, router_1.Router])
    ], ProductDetailsResolve);
    return ProductDetailsResolve;
}());
exports.ProductDetailsResolve = ProductDetailsResolve;
//# sourceMappingURL=product-detail-resolve.service.js.map