import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import {ProductsComponent, ProductDetailComponent} from "../index";
import {ProductDetailsResolve} from "./product-detail-resolve.service";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: "products", component: ProductsComponent },
            {
                path: "products/:id",
                component: ProductDetailComponent,
                resolve: {
                    product: ProductDetailsResolve
                }
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ProductsRoutingModule { }
