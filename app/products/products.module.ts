import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import {ProductDetailComponent, ProductsComponent} from "../index";
import {ProductDetailsResolve} from "./product-detail-resolve.service";
import {ProductsRoutingModule} from "./products-routes.module";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ProductsRoutingModule
    ],
    declarations: [
        ProductDetailComponent,
        ProductsComponent
    ],
    providers: [ProductDetailsResolve]
})
export class ProductsModule { }