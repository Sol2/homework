import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../shared/products.service";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'app-products',
    templateUrl: 'products.component.html',
    styleUrls: ['products.component.css']
})

export class ProductsComponent implements OnInit {
    products: {id: number, name: string, price: number, category: string}[];

    constructor(private _productsService: ProductsService,
                private _router: Router,
                private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.products = this._productsService.getProducts();
    }

    selectProduct(id) {
        console.log(id);
        this._router.navigate([id], {relativeTo: this._activatedRoute});
        return false;
    }
}